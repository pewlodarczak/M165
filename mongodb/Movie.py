class Movie:

    director = ''

    def __init__(self, title, type, genre):
        self.title, self.type, self.genre = title, type, genre

    def __repr__(self):
        return f"Title: {self.title} genre: {self.genre}"
    
    def get_movie(self):
        return self.title
    
    @classmethod
    def director(cls, director):
        cls.director = director

    
mov1 = Movie('The Mandalorian', 'Series', 'SciFy')
mov2 = Movie('Shazam', 'Film', 'Comedy')

print(mov1)
print(mov2)
print(mov1.get_movie())

Movie.director('Altman')
print(Movie.director)
mov1.director = 'Capra'
print(mov1.director)
print(Movie.director)
print(mov2.director)
# mov2.director = 'Spielberg'

# print(Movie.director)
# print(mov1.director)
# print(mov2.director)