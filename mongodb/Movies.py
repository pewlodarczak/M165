class Movies:

    movie_title = "Star trek"

    def __init__(self, title, type):
        self.movie_title, self.type = title, type

    def __repr__(self):
        return f"Title: {self.movie_title} type: {self.type}"
    
    @classmethod
    def get_movie(cls):
        return cls.movie_title

movie1 = Movies("Shazam", 'Film')
print(movie1)

movie2 = Movies('The Mandalorian', 'Serie')
print(movie2)

print(Movies.get_movie())