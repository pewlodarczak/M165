from pymongo.mongo_client import MongoClient
import rich as pprint
import pymongo.errors
from config import ConfigReader

class DBAccess:

    def __init__(self) -> None:
        try:
            self.params = ConfigReader(configfile='db.ini', section='booksdb').config()
            self.client = MongoClient(self.params['url'])
            self.db = self.client[self.params['db']]
        except pymongo.errors.ServerSelectionTimeoutError as e:
            print("Timeout connecting to MongoDB server: %s" % e)
        except pymongo.errors.ConnectionFailure as e:
            print("Error connecting to DB server: %s " % e)
    
    # Aufg 1
    def CreateBook(self, collection, book) -> None:
        collection = self.db[collection]
        result = collection.insert_one(book)
        print(result.inserted_id)


    # Aufg 3
    def ListAllBooks(self, collection) -> None:
        collection = self.db[collection]
        documents = collection.find({})
        num_results = collection.count_documents({})
        pprint.print(f"Found {num_results} books")

        for document in documents:
            pprint.print(document)

    # Aufg 4
    def ChangePrice(self, collection, title, new_price) -> None:
        collection = self.db[collection]
        query = { "title": title }
        new_price = { "$set": { "price": new_price } }
        result = collection.update_one(query, new_price)
        pprint.print(result.modified_count)

    # Aufg 5
    def ListPriceRange(self, collection, min_price, max_price) -> None:
        collection = self.db[collection]
        query = {'price': {"$gte": min_price, "$lte": max_price}}
        results = collection.find(query)
        for document in results:
            pprint.print(document)

    # Aufg 6
    def ListTitles(self, collection) -> None:
        collection = self.db[collection]
        projection = {"_id": 0, "title": 1}
        results = collection.find({}, projection)
        for document in results:
            pprint.print(document)

    # Aufg 7
    def DeleteBook(self, collection, title) -> None:
        collection = self.db[collection]
        query = {"title": title}
        result = collection.delete_one(query)
        pprint.print(result.deleted_count)

    # Aufg 8
    def DelCollection(self, collection) -> None:
        try:
            self.db[collection].drop()
        except pymongo.errors.CollectionInvalid as e:
            pprint.print(f"Error collectio {collection} does not exist")
            raise e

    def __del__(self):
        try:
            self.client.close()
            self.client = None
        except AttributeError as e:
            pprint.print(f"Error attribute does not exist")

def main():
    dbo = DBAccess()

    aBook = { "title": "1984", "author": 'George Orwell', "price": 18 }
    dbo.CreateBook('books', aBook)
    aBook = { "title": "Sapiens: A Brief History of Humankind", "author": 'Yuval Harari', "price": 25 }
    dbo.CreateBook('books', aBook)
    # dbo.ListAllBooks('books')
    # dbo.ChangePrice('books', '1984', 20)
    dbo.ListPriceRange('books', 18, 28)
    # dbo.ListTitles('books')
    # dbo.DeleteBook('books', '1984')
    # dbo.DelCollection('books')
    dbo = None

if __name__ == '__main__':
    main()        
