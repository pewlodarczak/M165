from configparser import ConfigParser

class ConfigReader():

    def __init__(self, configfile, section):
        self.configfile, self.section = configfile, section

    def config(self):
        parser = ConfigParser()
        list = parser.read(self.configfile)
        if len(list) == 0:
            print(f"{self.configfile} not found")
            raise Exception('{0} file not found'.format(self.configfile))

        db = {}
        if parser.has_section(self.section):
            params = parser.items(self.section)
            for param in params:
                db[param[0]] = param[1]
        else:
            raise Exception('Section {0} not found in the {1} file'.format(self.section, self.configfile))
        # print(db)
        return db
