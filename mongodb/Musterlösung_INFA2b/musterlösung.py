import rich as pprint
import logging
import pymongo.errors
from connect import Connect

class DBAccess:

    def __init__(self, db) -> None:
        logging.basicConfig(filename='error.log', level=logging.ERROR)
        try:
            self.client = Connect('db.ini', db).connect()
            self.db = self.client[db]
        except pymongo.errors.ServerSelectionTimeoutError as e:
            print("Timeout connecting to MongoDB server: %s" % e)
            logging.error(e)
        except pymongo.errors.ConnectionFailure as e:
            print("Error connecting to DB server: %s " % e)
            logging.error(e)
    
    # Aufg 1
    def NumofEntries(self, collection) -> None:
        try:
            collection = self.db[collection]
            num_results = collection.count_documents({})
            pprint.print(f"Found {num_results} results")
        except pymongo.errors.CollectionInvalid as e:
            pprint.print(f"Error collectio {collection} does not exist")
            logging.error(e)
        except pymongo.errors.ServerSelectionTimeoutError as e:
            print("Timeout connecting to MongoDB server: %s" % e)
            logging.error(e)
        except pymongo.errors.ConnectionFailure as e:
            print("Error connecting to DB server: %s " % e)
            logging.error(e)


    # Aufg 2
    def ListEntriesPerCountry(self, collection, country) -> None:
        try:
            collection = self.db[collection]
            documents = collection.find({'address.country': country})
            for document in documents:
                pprint.print(document)
            num_results = collection.count_documents({'address.country': country})
            pprint.print(f"Found {num_results} results in {country}")
        except pymongo.errors.CollectionInvalid as e:
            pprint.print(f"Error collectio {collection} does not exist")
            logging.error(e)
        except pymongo.errors.InvalidName as e:
            pprint.print(f"Error name {country} does not exist")
        except pymongo.errors.ServerSelectionTimeoutError as e:
            print("Timeout connecting to MongoDB server: %s" % e)
            logging.error(e)
        except pymongo.errors.ConnectionFailure as e:
            print("Error connecting to DB server: %s " % e)
            logging.error(e)


    # Aufg 3
    def FindByName(self, collection, name) -> None:
        try:
            collection = self.db[collection]
            query = { "name": name }
            document = collection.find_one(query)
            if document == None:
                print('Entry not found')
            else:
                pprint.print(document)
        except pymongo.errors.CollectionInvalid as e:
            pprint.print(f"Error collectio {collection} does not exist")
            logging.error(e)
        except pymongo.errors.InvalidName as e:
            pprint.print(f"Error name {name} does not exist")


    # Aufg 4
    def ChangeNumOfBeds(self, collection, name, beds) -> None:
        try:
            collection = self.db[collection]
            query = { "name": name }
            new_age = { "$set": { "beds": beds } }
            result = collection.update_one(query, new_age)
            pprint.print(result.modified_count)
        except pymongo.errors.CollectionInvalid as e:
            pprint.print(f"Error collectio {collection} does not exist")
            logging.error(e)
        except pymongo.errors.InvalidName as e:
            pprint.print(f"Error name {name} does not exist")

    # Aufg 5
    def ShowNumOfBeds(self, collection, name) -> None:
        query = { "name": name }
        try:
            collection = self.db[collection]
            projection = {"beds": 1}
            results = collection.find(query, projection)
            for document in results:
                pprint.print(document)
        except pymongo.errors.CollectionInvalid as e:
            pprint.print(f"Error collectio {collection} does not exist")
            logging.error(e)
        except pymongo.errors.InvalidName as e:
            pprint.print(f"Error name {name} does not exist")

    # Aufg 6
    def RangeOfBeds(self, collection, min_beds, max_beds, country) -> None:
        query = {"beds": {"$gte": min_beds, "$lte": max_beds}, "address.country": country}
        projection = {"name": 1, "address.country": 1, 'beds': 1}
        try:
            collection = self.db[collection]
            results = collection.find(query, projection)
            num_results = collection.count_documents(query)
            pprint.print(f"Found {num_results} results")
            for document in results:
                pprint.print(document)
        except pymongo.errors.CollectionInvalid as e:
            pprint.print(f"Error collectio {collection} does not exist")
            logging.error(e)
        except pymongo.errors.InvalidName as e:
            pprint.print(f"Error name {country} does not exist")


    # Aufg 7
    def CreateNewContact(self, collection, name, email) -> None:
        query = { "name": name, "email": email }
        try:
            collection = self.db[collection]
            result = collection.insert_one(query)
            print(f'Inserted with id {result.inserted_id}')
        except pymongo.errors.CollectionInvalid as e:
            pprint.print(f"Error collectio {collection} does not exist")
            logging.error(e)
        except pymongo.errors.InvalidName as e:
            pprint.print(f"Error name {name} does not exist")

    # Aufg 8
    def DelUser(self, collection, name) -> None:
        query = { "name": name }
        try:
            collection = self.db[collection]
            result = collection.delete_one(query)
            pprint.print(f'Deleted {result.deleted_count}')
        except pymongo.errors.CollectionInvalid as e:
            pprint.print(f"Error collectio {collection} does not exist")
            logging.error(e)
        except pymongo.errors.InvalidName as e:
            pprint.print(f"Error name {name} does not exist")

    # Aufg 9
    def DelCollection(self, collection):
        try:
            self.db[collection].drop()
        except pymongo.errors.CollectionInvalid as e:
            pprint.print(f"Error collectio {collection} does not exist")
            logging.error(e)
            raise e

    def __del__(self):
        try:
            self.client.close()
            self.client = None
        except AttributeError as e:
            pprint.print(f"Error attribute does not exist")
            logging.error(e)

def main():
    dbo = DBAccess('airbnb')
    dbo.NumofEntries('listingsAndReviews')
    dbo.ListEntriesPerCountry('listingsAndReviews', 'Portugal')
    dbo.FindByName('listingsAndReviews', 'Cozy Nest, heart of the Plateau')
    dbo.ChangeNumOfBeds('listingsAndReviews', 'Cozy Nest, heart of the Plateau', 1)
    dbo.ShowNumOfBeds('listingsAndReviews', 'Cozy Nest, heart of the Plateau')
    dbo.RangeOfBeds('listingsAndReviews', 2, 4, 'Portugal')
    dbo.CreateNewContact('contacts', 'Miller', 'miller@domain.com')
    dbo.CreateNewContact('contacts', 'Roberts', 'roberts@domain.com')
    dbo.DelUser('contacts', 'Roberts')
    dbo.DelCollection('contacts')
    dbo = None

if __name__ == '__main__':
    main()        
