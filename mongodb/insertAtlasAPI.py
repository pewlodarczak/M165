from pymongo.mongo_client import MongoClient
from pymongo.server_api import ServerApi
import pprint
import datetime

uri = "atlasuri"

client = MongoClient(uri, server_api=ServerApi('1'))

try:
    db = client.imageapp
    collection = db.users

    post = {"username": "Malcom",
        "phone": 654321,
        "tags": ["mongodb", "python", "pymongo"],
        "date": datetime.datetime.now()}

    collection_id = collection.insert_one(post).inserted_id
    print(collection_id)
    # print(db.list_collection_names())

except Exception as e:
    pprint("Error inserting:")
    pprint(e)

