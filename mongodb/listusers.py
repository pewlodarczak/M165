from pymongo.mongo_client import MongoClient
import pymongo
import pprint
import connect

class ConnectDB():

    def __init__(self):
        self.client = MongoClient('localhost', 27017)

    def listUsers(self):
        try:
            client = MongoClient('localhost', 27017)
            db = client['imageapp']
            collection = db.users

            # pprint.pprint(collection.find_one())
            
            cursor = collection.find({})
            for document in cursor:
                pprint.pprint(document)

        except pymongo.errors.ServerSelectionTimeoutError as e:
            print("Timeout connecting to MongoDB server: %s" % e)
            raise e
        except pymongo.errors.ConnectionFailure as e:
            pprint("Error connecting to DB server: %s " % e)
            raise e
    
    def insert(self, user):
        db = self.client['imageapp']
        collection = db.users
        try:
            x = collection.insert_one(user)
            print(x.inserted_id)
        except pymongo.errors.ServerSelectionTimeoutError as e:
            print("Timeout connecting to MongoDB server: %s" % e)
            raise e
        except pymongo.errors.ConnectionFailure as e:
            pprint("Error connecting to DB server: %s " % e)
            raise e
        
    def search(self):
        client = MongoClient()
        db = client['imageapp']
        collection = db['users']
        collection.create_index([('city', 'text')])
        results = collection.find({'$text': {'$search': 'San Agn'}})

        for document in results:
            pprint.pprint(document)

    def fullTextSearch(self):
        client = MongoClient()
        db = client['imageapp']
        collection = db['users']
        collection.create_index([('name', 'text'), ('city', 'text')])
        # collection.create_index([('field1', 'text'), ('field2', 'text'), ('field3', 'text')])
        results = collection.find({'$text': {'$search': 'San name city'}})

        for document in results:
            pprint.pprint(document)

    def __del__(self):
        self.client = None

aUser = { "name": "Agnes", "age": 35, "city": "Fran" }

def main():
    try:
        connObj = ConnectDB()
        connObj2  = ConnectDB()
        # connObj.insert(aUser)
        connObj.listUsers()
        # connObj.search()
        # connObj.fullTextSearch()

        connObj = None
        
    except pymongo.errors.ConnectionFailure as e:
        print("Could not connect to MongoDB server: %s" % e)


if __name__ == "__main__":
    main()

