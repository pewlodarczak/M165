import pprint
from pymongo import MongoClient

print(__name__)

# client = MongoClient()
client = MongoClient('localhost', 27017)
# or using URL
# client = MongoClient('mongodb://localhost:27017/')

db = client.imageapp
# or
# db = client['newdb2']

collection = db.users
# or
# collection = db['myCollection']

# pprint.pprint(collection.find_one())

cursor = collection.find({'name': 'Malcom'})
for document in cursor:
    pprint.pprint(document)

