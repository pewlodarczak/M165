import pprint
from pymongo import MongoClient

client = MongoClient()
client = MongoClient('atlasuri')

db = client.imageapp
# or
# db = client['newdb2']

collection = db.users
# or
# collection = db['myCollection']

pprint.pprint(collection.find_one())

cursor = collection.find({})
for document in cursor:
    pprint.pprint(document)
