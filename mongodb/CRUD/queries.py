from pymongo import MongoClient
from connect import Connect
from pprint import pprint

class Query:

    def __init__(self, section):
        self.client = Connect('db.ini', section).connect()
        self.db = self.client['movies']

    def ReadAllMovies(self, collection):
        # db = self.client['movies']
        collection = self.db['movies']
        cursor = collection.find()
        for document in cursor:
            print(document)

    def SearchOne(self, actor):
        collection = self.db['movies']
        cursor = collection.find({ 'cast': actor})
        for document in cursor:
            pprint(document)

    def SearchMany(self, collection):
        collection = self.db[collection]
        search_terms = ["Brad Pitt", "Robert De Niro"]
        query = {"cast": {"$in": search_terms}}
        results = collection.find(query, {"title": 1, "cast": 1})

        num_results = collection.count_documents(query)
        print(f"Found {num_results} results")
        for document in results:
            pprint(document)


qob = Query('moviesdb')
# qob.ReadAllMovies('movies')
qob.SearchOne('Robert De Niro')
