from pymongo import MongoClient

def Insert(user):
    client = MongoClient("mongodb://localhost:27017/")
    db = client["imageapp"]
    collection = db["users"]
    try:
        x = collection.insert_one(user)
        print(x.inserted_id)
    except Exception as e:
        print("Error inserting:")
        print(e)

def Read(user):
    client = MongoClient("mongodb://localhost:27017/")
    db = client["imageapp"]
    collection = db["users"]
    query = { "name": user }
    document = collection.find_one(query)
    if document == None:
        print('User not found')
    else:
        print(document)

def ReadAll(collection):
    client = MongoClient("mongodb://localhost:27017/")
    db = client["imageapp"]
    collection = db[collection]
    cursor = collection.find()
    for document in cursor:
        print(document)

def Update(user, age):
    client = MongoClient("mongodb://localhost:27017/")
    db = client["imageapp"]
    collection = db["users"]
    query = { "name": user }
    new_age = { "$set": { "age": age } }
    result = collection.update_one(query, new_age)

    # Print the number of documents updated
    print(result.modified_count)

def UpdateInsert(user, sex):
    client = MongoClient("mongodb://localhost:27017/")
    db = client["imageapp"]
    collection = db["users"]
    query = { "name": user }
    new_age = { "$set": { "sex": sex } }
    result = collection.update_one(query, new_age)

    # Print the number of documents updated
    print(result.modified_count)

def Delete(name):
    client = MongoClient("mongodb://localhost:27017/")
    db = client["imageapp"]
    collection = db["users"]
    query = { "name": "John" }
    result = collection.delete_one(query)

aUser = { "name": "John", "age": 30, "city": "New York" }
# aUser = { "name": "Malcom", "age": 45, "city": "Zug" }
# Insert(aUser)
# Read('John')
ReadAll('users')
# Update('John', 30)
# UpdateInsert('John', 'male')
# Delete('John')