from pymongo import MongoClient
from connect import Connect
from rich import print as rprint
from rich.console import Console
from os import system
import logging
import pymongo.errors

console = Console()

class CRUD():
    # factory method
    @classmethod
    def CRUD_factory(cls, db):
        return CRUD(db)

    def __init__(self, db):
        logging.basicConfig(filename='error.log', level=logging.ERROR)
        try:
            self.client = Connect('db.ini', db).connect()
        except pymongo.errors.ServerSelectionTimeoutError as e:
            print("Timeout connecting to MongoDB server: %s" % e)
            logging.error(e)
            raise e
        except pymongo.errors.ConnectionFailure as e:
            print("Error connecting to DB server: %s " % e)
            logging.error(e)
            raise e
        
        self.db = self.client[db]

    def Insert(self, user):
        collection = self.db["users"]
        try:
            x = collection.insert_one(user)
            print(x.inserted_id)
        except Exception as e:
            print("Error inserting:")
            print(e)

    def Read(self, user):
        collection = self.db["users"]
        query = { "name": user }
        document = collection.find_one(query)
        if document == None:
            print('User not found')
        else:
            print(document)

    def ReadAll(self, collection):
        try:
            collection = self.db[collection]
            cursor = collection.find()
            for document in cursor:
                print(document)
        except pymongo.errors.ServerSelectionTimeoutError as e:
            print("Timeout connecting to MongoDB server: %s" % e)
            logging.error(e)
        except pymongo.errors.ConnectionFailure as e:
            print("Error connecting to DB server: %s " % e)
            logging.error(e)

    def Update(self, user, age):
        collection = self.db["users"]
        query = { "name": user }
        new_age = { "$set": { "age": age } }
        result = collection.update_one(query, new_age)
        print(result.modified_count)

    def UpdateInsert(self, user, sex):
        collection = self.db["users"]
        query = { "name": user }
        new_age = { "$set": { "sex": sex } }
        result = collection.update_one(query, new_age)
        print(result.modified_count)

    def Delete(self, name):
        collection = self.db["users"]
        query = { "name": name }
        result = collection.delete_one(query)

    def __del__(self) -> None:
        self.client = None

def main():
    toDo = ''

    # app = CRUD.CRUD_factory('imageapp')
    # aUser = { "name": "John", "age": 30, "city": "New York" }
    # aUser = { "name": "Malcom", "age": 45, "city": "Zug" }
    # aUser = { "name": "Jane", "age": 25, "city": "Luzern" }

    try:
        app = CRUD('imageapp')
    except pymongo.errors.ServerSelectionTimeoutError as e:
        print("Timeout connecting to MongoDB server: %s" % e)
        logging.error(e)
    except pymongo.errors.ConnectionFailure as e:
        print("Error connecting to DB server: %s " % e)
        logging.error(e)

    aUser = { "name": "Gwendolin", "age": 55, "city": "Baar" }

    while toDo != 'q':
        print('What do you want to do:')
        print('l\tList all users')
        print('i\tInsert a user')
        print('d\tDelete a user')
        print('q\tQuit')

        toDo = input()
        if toDo == 'l':
            app.ReadAll('users')
        elif toDo == 'i':
            app.Insert(aUser)
        elif toDo == 'd':
            app.Delete('Gwendolin')
            system('cls')
        elif toDo == 'q':
            print()
            # rprint(['Au revoir', '😎'])
            print('👋')
            # console.print(":thumbs_up: Hasta la vista, baby")
            # rprint('Hasta la vista, baby', '👽')
            console.print('Hasta la vista, baby', style="blink bold red underline on white")
            print()
        else:
            print('wrong command')

'''
    while toDo != '4':
        dbGUI.printMenu(num)

        toDo = input()
        if toDo == 'c':
            imgDAO.create_table()
        elif toDo == 'd':
            imgDAO.drop_table()
        elif toDo == '1':
            imgDAO.insert_image("Flip", 'img/flip.gif', "Flip")
            num = 1
            system('cls')
        elif toDo == '2':
            imgDAO.read_image('Flip')
            num = 2
            system('cls')
        elif toDo == '3':
            imgDAO.delete_image(7)
        elif toDo == '4':
            print()
            #rprint(['Au revoir', '😎'])
            print('👋')
            #console.print(":thumbs_up: Hasta la vista, baby")
            #rprint('Hasta la vista, baby', '👽')
            console.print('Hasta la vista, baby', style="blink bold red underline on white")
            print()
        else:
            print('wrong command')
'''

if __name__ == '__main__':
    main()

# Read('John')

# Update('John', 30)
# UpdateInsert('John', 'male')
# Delete('John')
