from pymongo import MongoClient
from pprint import pprint

class DBObj:

    def __init__(self, host, db):
        self.client = MongoClient(host)
        self.db = self.client[db]

    def ReadAll(self, collection):
        collection = self.db[collection]
        cursor = collection.find()
        for document in cursor:
            pprint(document)

    def InsertUser(self, collection, user):
        self.db[collection].insert_one(user)

    def DeleteUser(self, collection, first_name, last_name):
        self.db[collection].delete_one({ "last_name": last_name, 'first_name': first_name })

    def Query(self, collection, first_name, last_name):
        document = self.db[collection].find_one({ "last_name": last_name, 'first_name': first_name })
        if document == None:
            print('User not found')
        else:
            pprint(document)

    def __del__(self):
        self.client.close()
        self.client = None


def main():
    dbo1 = DBObj('mongodb://localhost:27017/', 'imageapp')
    # dbo1.ReadAll('users')

    aUser = { "first_name": "John", "last_name": "Flinn", "age": 30, "city": "New York" }
    # dbo1.InsertUser('users', aUser)
    # dbo1.ReadAll('users')
    # dbo1.DeleteUser('users', 'John', 'Flinn')
    dbo1.ReadAll('users')
    dbo1.Query('users', 'John', 'Flinn')
    dbo1 = None

    # dbo2  = DBObj('mongodb://localhost:27017/', 'movies')
    # dbo2.ReadAll('users')

if __name__ == "__main__":
    main()