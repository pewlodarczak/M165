from connect import Connect
from rich import print as rprint
from rich.console import Console
from pprint import pprint
import logging
import pymongo.errors

console = Console()

class QueryObj():

    def __init__(self, section) -> None:
        logging.basicConfig(filename='error.log', level=logging.ERROR)
        try:
            self.client = Connect('db.ini', section).connect()
        except pymongo.errors.ServerSelectionTimeoutError as e:
            print("Timeout connecting to MongoDB server: %s" % e)
            logging.error(e)
            raise e
        except pymongo.errors.ConnectionFailure as e:
            print("Error connecting to DB server: %s " % e)
            logging.error(e)
            raise e
        
        self.db = self.client['movies']

    def Query(self, collection):
        collection = self.db[collection]
        search_terms = ["Brad Pitt", "Robert De Niro"]
        query = {"cast": {"$in": search_terms}}
        results = collection.find(query, {"title": 1, "cast": 1})

        num_results = collection.count_documents(query)
        print(f"Found {num_results} results")
        print("List them? Y/N")
        inp = input()
        if inp == "Y":
            for document in results:
                pprint(document)
            
    def ListAll(self, collection):
        collection = self.db[collection]
        cursor = collection.find()
        for document in cursor:
            print(document)

    def __del__(self) -> None:
        self.client = None

def main():
    toDo = ''

    app = QueryObj('moviesdb')

    while toDo != 'q':
        print('What do you want to do:')
        print('l\tEnter query')
        print('q\tQuit')

        toDo = input()
        if toDo == 'l':
            app.Query('movies')
        elif toDo == 'q':
            rprint(['Au revoir', '😎'])
            print('👋')
            print()
        else:
            print('wrong command')


if __name__ == '__main__':
    main()
