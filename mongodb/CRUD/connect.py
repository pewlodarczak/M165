from pymongo.mongo_client import MongoClient
import pymongo
from config import ConfigReader
import pprint
import logging

class Connect:

    def __init__(self, configfile, section):
        logging.basicConfig(filename='error.log', level=logging.ERROR)
        try:
            self.params = ConfigReader(configfile='db.ini', section='mongodb').config()
        except Exception as e:
            print(e)
    
    def connect(self):
        try:
            client = MongoClient(self.params['url'])
            return client
        except pymongo.errors.ServerSelectionTimeoutError as e:
            raise e
        except pymongo.errors.ConnectionFailure as e:
            raise e

def main():
    try:
        Connect('db.ini', 'mongodb').connect()
    except pymongo.errors.ServerSelectionTimeoutError as e:
        print("Timeout connecting to MongoDB server: %s" % e)
        logging.error(e)
        raise e
    except pymongo.errors.ConnectionFailure as e:
        pprint("Error connecting to DB server: %s " % e)
        logging.error(e)
        raise e

main()

if __name__ == '__MAIN__':
    main()