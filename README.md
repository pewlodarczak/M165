# Prerequisites:

Python Version 3.x, MongoDB 6.x

# MongoDB
Die Python und pip Version überprüfen

```batch
python --version
pip --version
```
Python muss mindestens Version 3.x sein. <br>
Ein virtuelles Envorinment erstellen und aktivieren:

```batch
python -m venv env
env\scripts\activate
```

Sicherstellen dass `pip` installiert ist:

```batch
python -m pip install --upgrade pip
```

Den MongoDB Treiber, rich und config installieren:

```batch
pip install pymongo rich config
```

Das Repository clonen:

```batch
git clone https://gitlab.com/pewlodarczak/m165.git
```

Das Script starten:

```batch
cd m165
cd mongodb
cd CRUD
python CRUDObj.py
```
