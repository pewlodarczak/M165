from neo4j import GraphDatabase, basic_auth
# from neo4j.graph import Node, Relationship
from rich import print as pprint
import yaml
import logging
from neo4j.exceptions import ServiceUnavailable, CypherSyntaxError, AuthError, Neo4jError
from tabulate import tabulate
from colorama import Fore, Style

__author__    = "Author: PWLO"
__version__   = "Revision: 0.1"
__date__      = "Date: 2023-06-23 18:09:43 +0200 (Fr, 16 jun 2023)"


class DBQueries:
        
    def __init__(self, config) -> None:
        self.logger = logging.getLogger('error_logger')
        self.logger.setLevel(logging.ERROR)
        formatter = logging.Formatter('[%(asctime)s] %(levelname)s: %(message)s', '%Y-%m-%d %H:%M:%S')
        file_handler = logging.FileHandler('error.log')
        file_handler.setFormatter(formatter)            
        self.logger.addHandler(file_handler)

        self.loggerinfo = logging.getLogger('info_logger')
        self.loggerinfo.setLevel(logging.INFO)
        self.loggerinfo.addHandler(file_handler)

        self.loggerwarn = logging.getLogger('warn_logger')
        self.loggerwarn.setLevel(logging.WARN)
        self.loggerwarn.addHandler(file_handler)

        file_path = 'access.yaml'

        with open(file_path, 'r') as file:
            yaml_data = yaml.load(file, Loader=yaml.FullLoader)

        uri = yaml_data[config]['uri']
        username = yaml_data[config]['username']
        password = yaml_data[config]['password']
        try:
            self.driver = GraphDatabase.driver(uri, auth=basic_auth(username, password))
            with self.driver.session() as session:
                result = session.run("""CALL dbms.components() 
                    YIELD name, versions
                    WHERE name = 'Neo4j Kernel'
                    RETURN versions[0] AS version;""")
                pprint(result.data())
        except ServiceUnavailable as e:
            self.logger.error(e)
        except AuthError as e:
            print("Authentication failed:", e)
            self.logger.error(e)
        except Neo4jError as e:
            print("Neo4j error:", e)
            self.logger.error(e)
        except Exception as e:
            print("An error occurred:", e)
            self.logger.error(e)

    # Aufg 2
    def get_all_nodes(self):
        query = """
            MATCH (n) RETURN n
        """
        try:
            with self.driver.session() as session:
                nodes = session.run(query)
                summary = 0
                for n in nodes:
                    pprint(n)
                    summary += 1
                self.loggerinfo.info(f"{summary} nodes have been returned")
                print()
                print(f"{Fore.RED}Total nodes: {summary}{Style.RESET_ALL}")
        except ServiceUnavailable as exception:
            self.logger.error("{query} raised an error: \n {exception}".format(
                query=query, exception=exception))
        except CypherSyntaxError as exception:
            self.logger.error("{query} raised an error: \n {exception}".format(
                query=query, exception=exception))
            raise
    
    # Aufg 3, 4
    def get_products_for_category(self, category):
        query = """
            MATCH (c:Category)<-[r]-(p:Product) WHERE c.categoryName = $category RETURN p.productName, p.unitPrice
        """
        try:
            with self.driver.session() as session:
                nodes = session.run(query, category=category)
                summary = 0
                for n in nodes:
                    pprint(n['p.productName'], n['p.unitPrice'])
                    summary += 1
                self.loggerinfo.info(f"{summary} Products have been returned")
                print()
                print(f"{Fore.RED}Total Products: {summary}{Style.RESET_ALL}")
        except ServiceUnavailable as exception:
            self.logger.error("{query} raised an error: \n {exception}".format(
                query=query, exception=exception))
        except CypherSyntaxError as exception:
            self.logger.error("{query} raised an error: \n {exception}".format(
                query=query, exception=exception))
            raise

    # Aufg 5
    def get_supplier_for_category(self, category):
        query = """
            MATCH (c:Category)<-[r]-(p:Product)<-[u]-(s:Supplier) WHERE c.categoryName = $category RETURN s.companyName, s.city
        """
        try:
            with self.driver.session() as session:
                nodes = session.run(query, category=category)
                summary = 0
                for n in nodes:
                    pprint(n['s.companyName'], n['s.city'])
                    summary += 1
                self.loggerinfo.info(f"{summary} Suppliers have been returned")
                print()
                print(f"{Fore.RED}Total Supplier: {summary}{Style.RESET_ALL}")
        except ServiceUnavailable as exception:
            self.logger.error("{query} raised an error: \n {exception}".format(
                query=query, exception=exception))
        except CypherSyntaxError as exception:
            self.logger.error("{query} raised an error: \n {exception}".format(
                query=query, exception=exception))
            raise

    # Aufg 6
    def get_supplier_of_product(self, city, productName):
        query = """
            MATCH (p:Product {productName: $productName})<-[r]-(s:Supplier {city: $city}) RETURN s.companyName
        """
        try:
            with self.driver.session() as session:
                nodes = session.run(query, city=city, productName=productName)
                summary = 0
                for n in nodes:
                    pprint(n['s.companyName'])
                    summary += 1
                self.loggerinfo.info(f"{summary} Suppliers have been returned")
                print()
                print(f"{Fore.RED}Total Supplier: {summary}{Style.RESET_ALL}")
        except ServiceUnavailable as exception:
            self.logger.error("{query} raised an error: \n {exception}".format(
                query=query, exception=exception))
        except CypherSyntaxError as exception:
            self.logger.error("{query} raised an error: \n {exception}".format(
                query=query, exception=exception))
            raise

    # Aufg 7
    def search_all_nodes_by_text(self, searchString):
        query = """
        MATCH (n) WHERE any(prop in keys(n) WHERE toString(n[prop]) CONTAINS $searchStr) RETURN n
        """
        with self.driver.session() as session:
            result = session.run(query, searchStr=searchString)
            for record in result:
                node = record["n"]
                print(node)

    def ShowProductsPerCustomer(self, customerName, city):
        query = """
        MATCH (c:Customer {companyName: $customerName, city: $city})-[p:PURCHASED]->(o:Order)-[i:ORDERS]->(m:Product) RETURN c, m.productName, m.unitPrice
        """
        try:
            data = [["Product Name", "Price"]]
            with self.driver.session() as session:
                result = session.run(query, customerName=customerName, city=city)
                for record in result:
                    # pprint.print(record['m.productName'])
                    # pprint.print(record['m.unitPrice'])
                    # print()
                    data.append([record['m.productName'],record['m.unitPrice']])
            print()
            header_color = Fore.YELLOW
            data_color = Fore.CYAN
            reset_color = Style.RESET_ALL
            formatted_data = [[f"{header_color}{cell}{reset_color}" if index == 0 else f"{data_color}{cell}{reset_color}" for index, cell in enumerate(row)]
                for row in data
            ]
            table = tabulate(formatted_data, headers="firstrow", tablefmt="fancy_grid")
            print(table)
        except ServiceUnavailable as exception:
            self.logger.error("{query} raised an error: \n {exception}".format(
                query=query, exception=exception))
        except CypherSyntaxError as exception:
            self.logger.error("{query} raised an error: \n {exception}".format(
                query=query, exception=exception))
            raise

    # Aufg 11
    def __del__(self) -> None:
        self.driver.close()

def main() -> None:
    dba = DBQueries('instance1')
    try:
        dba.get_all_nodes()
        # dba.get_products_for_category('Condiments')
        # dba.get_supplier_for_category('Condiments')
        # dba.get_supplier_of_product('New Orleans', 'Chef Anton\'s Cajun Seasoning')
        # dba.search_all_nodes_by_text('New Orleans')
        # dba.ShowProductsPerCustomer('Chop-suey Chinese', 'Bern')
    finally:
        del dba

if __name__ == '__main__':
    main()
