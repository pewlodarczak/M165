from neo4j import GraphDatabase, basic_auth
from rich import print as pprint
import yaml
import logging
from neo4j.exceptions import ServiceUnavailable, CypherSyntaxError, AuthError, Neo4jError
from tabulate import tabulate
from colorama import Fore, Style

__author__    = "Author: PWLO"
__version__   = "Revision: 0.1"
__date__      = "Date: 2023-06-23 18:09:43 +0200 (Fr, 16 jun 2023)"


class DBQueries:
        
    def __init__(self, config) -> None:
        self.logger = logging.getLogger('error_logger')
        self.logger.setLevel(logging.ERROR)
        formatter = logging.Formatter('[%(asctime)s] %(levelname)s: %(message)s', '%Y-%m-%d %H:%M:%S')
        file_handler = logging.FileHandler('error.log')
        file_handler.setFormatter(formatter)            
        self.logger.addHandler(file_handler)

        self.loggerinfo = logging.getLogger('info_logger')
        self.loggerinfo.setLevel(logging.INFO)
        self.loggerinfo.addHandler(file_handler)

        self.loggerwarn = logging.getLogger('warn_logger')
        self.loggerwarn.setLevel(logging.WARN)
        self.loggerwarn.addHandler(file_handler)

        file_path = 'access.yaml'

        with open(file_path, 'r') as file:
            yaml_data = yaml.load(file, Loader=yaml.FullLoader)

        uri = yaml_data[config]['uri']
        username = yaml_data[config]['username']
        password = yaml_data[config]['password']
        try:
            self.driver = GraphDatabase.driver(uri, auth=basic_auth(username, password))
            with self.driver.session() as session:
                result = session.run("""CALL dbms.components() 
                    YIELD name, versions
                    WHERE name = 'Neo4j Kernel'
                    RETURN versions[0] AS version;""")
                pprint(result.data())
        except ServiceUnavailable as e:
            self.logger.error(e)
        except AuthError as e:
            print("Authentication failed:", e)
            self.logger.error(e)
        except Neo4jError as e:
            print("Neo4j error:", e)
            self.logger.error(e)
        except Exception as e:
            print("An error occurred:", e)
            self.logger.error(e)

    def get_all_nodes(self):
        query = """
            MATCH (n) RETURN n
        """
        try:
            with self.driver.session() as session:
                nodes = session.run(query)
                summary = 0
                for n in nodes:
                    pprint(n)
                    summary += 1
                self.loggerinfo.info(f"{summary} nodes have been returned")
                print()
                print(f"{Fore.RED}Total nodes: {summary}{Style.RESET_ALL}")
        except ServiceUnavailable as exception:
            self.logger.error("{query} raised an error: \n {exception}".format(
                query=query, exception=exception))
        except CypherSyntaxError as exception:
            self.logger.error("{query} raised an error: \n {exception}".format(
                query=query, exception=exception))
            raise

    def get_nodes_and_relationships(self):
        cypher_query = """
            MATCH (n)
            RETURN count(n) AS totalNodes
            """
        query = """
            MATCH ()-[r]->()
            RETURN count(r) AS totalRelationships"""
        try:
            with self.driver.session() as session:
                result = session.run(cypher_query)
                total_nodes = result.single()["totalNodes"]
                print("Total Nodes:", total_nodes)
            with self.driver.session() as session:
                result = session.run(query)
                total_relationships = result.single()["totalRelationships"]
                print("Total Relationships:", total_relationships)
                self.loggerinfo.info(f"{total_nodes} nodes have been returned")
                self.loggerinfo.info(f"{total_relationships} relations have been returned")
        except ServiceUnavailable as exception:
            self.logger.error("{query} raised an error: \n {exception}".format(
                query=query, exception=exception))
        except CypherSyntaxError as exception:
            self.logger.error("{query} raised an error: \n {exception}".format(
                query=query, exception=exception))
            raise
    
    def get_supplier_by_city(self, city) -> None:
        query = """
            MATCH (s:Supplier) WHERE s.city CONTAINS $city RETURN s.companyName"""
        try:
            with self.driver.session() as session:
                nodes = session.run(query, city=city)
                for n in nodes:
                    self.loggerinfo.info(f"Supplier: {n['s.companyName']} Supplier in {city}")
                    print()
                    print(f"{Fore.GREEN}Supplier: {n['s.companyName']} Supplier in {city}{Style.RESET_ALL}")
        except ServiceUnavailable as exception:
            self.logger.error("{query} raised an error: \n {exception}".format(
                query=query, exception=exception))
        except CypherSyntaxError as exception:
            self.logger.error("{query} raised an error: \n {exception}".format(
                query=query, exception=exception))
            raise

    def products_by_supplier(self, supplier_name) -> None:
        query = """
            MATCH (s:Supplier)-[r]->(p:Product) WHERE s.companyName CONTAINS $supplier_name RETURN s.companyName, r, p.productName"""
        sum = 0
        with self.driver.session() as session:
            nodes = session.run(query, supplier_name=supplier_name)
            for n in nodes:
                sum += 1
                self.loggerinfo.info(f"Supplier: {n['s.companyName']} supplies {n['p.productName']}")
                print()
                print(f"{Fore.GREEN}Supplier: {n['s.companyName']} supplies {n['p.productName']}{Style.RESET_ALL}")
            print(sum)

    def customers_by_shipper(self, customer_name) -> None:
        query = """
            MATCH (s:Shipper)-[r]->(o:Order)-[p]-(c:Customer) WHERE s.companyName CONTAINS $customer_name RETURN DISTINCT s.companyName, c.companyName"""
        sum = 0
        headers = ["Customer", "is shipped by"]
        data = []
        with self.driver.session() as session:
            nodes = session.run(query, customer_name=customer_name)
            for n in nodes:
                sum += 1
                data.append([n['c.companyName'], n['s.companyName']])

                print()
                print(f"{Fore.GREEN}Customer: {n['c.companyName']} is shipped by {n['s.companyName']}{Style.RESET_ALL}")
            print(sum)

        # print(tabulate(data, headers=headers))
        colored_table = tabulate(data, headers=headers, tablefmt="plain")
        colored_table = "\n".join(f"\033[34m{row}\033[0m" for row in colored_table.splitlines())
        print(colored_table)

    def customer_of_employee(self, firstName, lastName):
        query = """
            MATCH (e:Employee)-[r]->(o:Order)-[p]-(c:Customer) WHERE e.firstName = $firstName AND e.lastName = $lastName RETURN DISTINCT c.companyName"""
        sum = 0
        with self.driver.session() as session:
            nodes = session.run(query, firstName=firstName, lastName=lastName)
            for n in nodes:
                sum += 1
                print()
                print(f"{Fore.GREEN}Customer: {n['c.companyName']}{Style.RESET_ALL}")
            print(sum)

    def employees(self, title, country):
        query = """
            MATCH (e:Employee) WHERE e.title = 'Sales Representative' AND e.country = 'USA' RETURN e.firstName AS `Given Name`, e.lastName AS `Family Name`"""
        sum = 0
        with self.driver.session() as session:
            nodes = session.run(query)
            header = nodes.keys()
            header_string = " | ".join(header)
            print(header_string)
            for n in nodes:
                sum += 1
                print(f"{Fore.GREEN}{n['Given Name']}\t     {n['Family Name']}{Style.RESET_ALL}")
            print(sum)

    def __del__(self) -> None:
        self.driver.close()

def main() -> None:
    dba = DBQueries('instance1')
    try:
        # dba.get_all_nodes()
        # dba.get_nodes_and_relationships() 
        # dba.get_supplier_by_city('Boston')
        # dba.products_by_supplier('Bigfoot Breweries')
        # dba.customers_by_shipper('Speedy Express')
        dba.customer_of_employee('Steven', 'Buchanan')
        # dba.employees(title='Sales Representative', country='USA')
    finally:
        del dba


if __name__ == '__main__':
    main()
