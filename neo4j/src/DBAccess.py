from neo4j import GraphDatabase, basic_auth
import rich as pprint
import yaml
import os
from urllib.parse import urlparse

class DBAccess:

    # queries
    # MATCH (p:Person {name: 'Alice'})-[r:FRIENDS_WITH]->(m:Person) RETURN p, r, m

    def __init__(self) -> None:
        file_path = 'access.yaml'

        with open(file_path, 'r') as file:
            yaml_data = yaml.load(file, Loader=yaml.FullLoader)

        uri = yaml_data['instance1']['uri']
        username = yaml_data['instance1']['username']
        password = yaml_data['instance1']['password']
        self.driver = GraphDatabase.driver(uri, auth=basic_auth(username, password))

    def __repr__(self):
        return f"Address: {self.driver.get_server_info().address.host}, Port: {self.driver.get_server_info().address.port}"
        
    def get_all_people(self) -> None:
        query = """MATCH (p:Person) RETURN p"""
        with self.driver.session() as session:
            result = session.run(query)
            for record in result:
                pprint.print(record)

            pprint.print(result)

    def create_person(self, name: str) -> None:
        query = """CREATE (p:Person {name: $name})"""
        pass

    def CreateDB(self) -> None:
        query = """
        MERGE (alice:Person {name: 'Alice', age: 30})
        MERGE (bob:Person {name: 'Bob'})
        MERGE (carol:Person {name: 'Carol'})
        MERGE (dave:Person {name: 'Dave'})

        MERGE (alice)-[:FRIENDS_WITH]->(bob)
        MERGE (bob)-[:FRIENDS_WITH]->(carol)
        MERGE (carol)-[:FRIENDS_WITH]->(dave)
        """
        with self.driver.session() as session:
            result = session.run(query)
            pprint.print(result)

    def UpdatePerson(self):
        query = """
        MERGE (alice:Person {name: 'Alice', age: 35, nationality: 'CH'})
        """
        with self.driver.session() as session:
            result = session.run(query)
            pprint.print(result)

    def FindAllPersons(self):
        with self.driver.session() as session:
            result = session.run(f"MATCH (n:Person) RETURN n LIMIT 5")
            for record in result:
                pprint.print(record)

    def FindFriends(self, person):
        query = """
        MATCH (p:Person {name: $name})-[:FRIENDS_WITH]->(friend:Person)
        RETURN friend.name AS friend_name
        """
        parameter = {"name": person}
        with self.driver.session() as session:
            result = session.run(query, parameter)
            for f in result:
                pprint.print(f)
                print()

    def LoadData(self):
        # Orders
        query1 = """
        LOAD CSV WITH HEADERS FROM 'https://gist.githubusercontent.com/jexp/054bc6baf36604061bf407aa8cd08608/raw/8bdd36dfc88381995e6823ff3f419b5a0cb8ac4f/orders.csv' AS row
        MERGE (order:Order {orderID: row.OrderID})
        ON CREATE SET order.shipName = row.ShipName;
        """

        # Products
        query2 = """
        LOAD CSV WITH HEADERS FROM 'https://gist.githubusercontent.com/jexp/054bc6baf36604061bf407aa8cd08608/raw/8bdd36dfc88381995e6823ff3f419b5a0cb8ac4f/products.csv' AS row
        MERGE (product:Product {productID: row.ProductID})
        ON CREATE SET product.productName = row.ProductName, product.unitPrice = toFloat(row.UnitPrice);
        """

        # Suppliers
        query3 = """
        LOAD CSV WITH HEADERS FROM 'https://gist.githubusercontent.com/jexp/054bc6baf36604061bf407aa8cd08608/raw/8bdd36dfc88381995e6823ff3f419b5a0cb8ac4f/suppliers.csv' AS row
        MERGE (supplier:Supplier {supplierID: row.SupplierID})
        ON CREATE SET supplier.companyName = row.CompanyName;
        """

        # Employees
        query4 = """
        LOAD CSV WITH HEADERS FROM 'https://gist.githubusercontent.com/jexp/054bc6baf36604061bf407aa8cd08608/raw/8bdd36dfc88381995e6823ff3f419b5a0cb8ac4f/employees.csv' AS row
        MERGE (e:Employee {employeeID:row.EmployeeID})
        ON CREATE SET e.firstName = row.FirstName, e.lastName = row.LastName, e.title = row.Title;        
        """

        # categories
        query5 = """
        LOAD CSV WITH HEADERS FROM 'https://gist.githubusercontent.com/jexp/054bc6baf36604061bf407aa8cd08608/raw/8bdd36dfc88381995e6823ff3f419b5a0cb8ac4f/categories.csv' AS row
        MERGE (c:Category {categoryID: row.CategoryID})
        ON CREATE SET c.categoryName = row.CategoryName, c.description = row.Description;        
        """

        # node list
        node_list = [query1, 
                     query2, 
                     query3, 
                     query4, 
                     query5]

        # indices
        ind_list = [
            "CREATE INDEX product_id FOR (p:Product) ON (p.productID);",
            "CREATE INDEX product_name FOR (p:Product) ON (p.productName);",
            "CREATE INDEX supplier_id FOR (s:Supplier) ON (s.supplierID);",
            "CREATE INDEX employee_id FOR (e:Employee) ON (e.employeeID);",
            "CREATE INDEX category_id FOR (c:Category) ON (c.categoryID);",
            "CREATE CONSTRAINT order_id FOR (o:Order) REQUIRE o.orderID IS UNIQUE;",
            "CALL db.awaitIndexes();",
        ]

        # relationships
        # orders and products
        rel1 = """
        LOAD CSV WITH HEADERS FROM 'https://gist.githubusercontent.com/jexp/054bc6baf36604061bf407aa8cd08608/raw/8bdd36dfc88381995e6823ff3f419b5a0cb8ac4f/orders.csv' AS row
        MATCH (order:Order {orderID: row.OrderID})
        MATCH (product:Product {productID: row.ProductID})
        MERGE (order)-[op:CONTAINS]->(product)
        ON CREATE SET op.unitPrice = toFloat(row.UnitPrice), op.quantity = toFloat(row.Quantity);        
        """

        # orders and employees
        rel2 = """
        LOAD CSV WITH HEADERS FROM 'https://gist.githubusercontent.com/jexp/054bc6baf36604061bf407aa8cd08608/raw/8bdd36dfc88381995e6823ff3f419b5a0cb8ac4f/orders.csv' AS row
        MATCH (order:Order {orderID: row.OrderID})
        MATCH (product:Product {productID: row.ProductID})
        MERGE (order)-[op:CONTAINS]->(product)
        ON CREATE SET op.unitPrice = toFloat(row.UnitPrice), op.quantity = toFloat(row.Quantity);
        """

        # products and suppliers
        rel3 = """
        LOAD CSV WITH HEADERS FROM 'https://gist.githubusercontent.com/jexp/054bc6baf36604061bf407aa8cd08608/raw/8bdd36dfc88381995e6823ff3f419b5a0cb8ac4f/products.csv
        ' AS row
        MATCH (product:Product {productID: row.ProductID})
        MATCH (supplier:Supplier {supplierID: row.SupplierID})
        MERGE (supplier)-[:SUPPLIES]->(product);
        """

        # products and categories
        rel4 = """
        LOAD CSV WITH HEADERS FROM 'https://gist.githubusercontent.com/jexp/054bc6baf36604061bf407aa8cd08608/raw/8bdd36dfc88381995e6823ff3f419b5a0cb8ac4f/products.csv
        ' AS row
        MATCH (product:Product {productID: row.ProductID})
        MATCH (category:Category {categoryID: row.CategoryID})
        MERGE (product)-[:PART_OF]->(category);
        """

        # employees (reporting hierarchy)
        rel5 = """
        LOAD CSV WITH HEADERS FROM 'https://gist.githubusercontent.com/jexp/054bc6baf36604061bf407aa8cd08608/raw/8bdd36dfc88381995e6823ff3f419b5a0cb8ac4f/employees.csv' AS row
        MATCH (employee:Employee {employeeID: row.EmployeeID})
        MATCH (manager:Employee {employeeID: row.ReportsTo})
        MERGE (employee)-[:REPORTS_TO]->(manager);
        """

        rel_list = [rel1, rel2, rel3, rel4, rel5]

        with self.driver.session() as session:
            # nodes
            # for node in node_list:
            #     result = session.run(node)
            #     pprint.print(result)

            # indices
            # for ind in ind_list:
            #     result = session.run(ind)
            #     pprint.print(result)

            # relationships
            for rel in rel_list:
                result = session.run(rel)
                pprint.print(result)

    def __del__(self) -> None:
        self.driver.close()

def main(argv):
    db = DBAccess()
    db.LoadData()
    # db.UpdatePerson()
    # db.FindAllPersons()

    # db.CreateDB()
    # db.UpdatePerson()
    # print(db)
    # db.FindFriends('Alice')
    # db.get_all_people()
    # db.FindAllPersons()

if __name__ == "__main__":
    main('Accessing Neo4j')