from neo4j import GraphDatabase, basic_auth
import yaml

class Neo4j:
    def __init__(self, config):

        try:
            from neo4j import GraphDatabase
            print("Neo4j driver has been successfully loaded.")
        except ImportError as e:
            print(f"Failed to import Neo4j driver: {e}")

        file_path = 'access.yaml'

        with open(file_path, 'r') as file:
            yaml_data = yaml.load(file, Loader=yaml.FullLoader)

        uri = yaml_data[config]['uri']
        username = yaml_data[config]['username']
        password = yaml_data[config]['password']
        self.driver = GraphDatabase.driver(uri, auth=basic_auth(username, password))
        try:
            with self.driver.session() as session:
                result = session.run("RETURN 1")

                if result.single()[0] == 1:
                    print("Connection to Neo4j database established successfully.")
                else:
                    print("Connection test failed.")
        except Exception as e:
            print(f"Failed to connect to Neo4j database: {e}")
        finally:
            self.driver.close()

    def __del__(self):
        self.driver.close()

def main():
    neo4j = Neo4j('instance1')
    neo4j.driver.close()

if  __name__ == '__main__':
    main()
