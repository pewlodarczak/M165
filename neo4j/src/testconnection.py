from neo4j import GraphDatabase, basic_auth
from rich import print as pprint
import yaml

class NAccess:

    def __init__(self) -> None:
        
        file_path = 'access.yaml'

        with open(file_path, 'r') as file:
            yaml_data = yaml.load(file, Loader=yaml.FullLoader)

        uri = yaml_data['instance1']['uri']
        username = yaml_data['instance1']['username']
        password = yaml_data['instance1']['password']

        self.driver = GraphDatabase.driver(uri, auth=basic_auth(username, password))

    def ListPerson(self, person):
        with self.driver.session() as session:
            result = session.run("""MATCH (person:Person {name: $person}) 
            RETURN person.name AS name, person.born AS born""", person=person)
            for record in result:
                pprint(record['name'], record['born'])

    def FindMoviesForPerson(self, person):
        with self.driver.session() as session:
            result = session.run("""MATCH (p:Person {name: $person})
            -[r:ACTED_IN]->(m:Movie) RETURN p.name AS name, type(r) AS rel_label , m.title AS title""", person=person)
            for record in result:
                pprint(record['name'], record['rel_label'], record['title'])

    def __del__(self):
        self.driver.close()


def main():
    nao = NAccess()
    nao.ListPerson('Tom Hanks')
    nao.FindMoviesForPerson('Tom Hanks')

if __name__ == '__main__':
    main()