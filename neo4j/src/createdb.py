from connect import Neo4j
from rich import print as pprint

"""
TODO:
    - Delete single person
"""

class CreateDB:

    def __init__(self):
        db = Neo4j('instance1')
        self.connection = db.driver

    def __repr__(self):
        return f"Address: {self.connection.get_server_info().address.host}, Port: {self.connection.get_server_info().address.port}"

    def find_persons(self, person):
        with self.connection.session() as session:
            result = session.run("""MATCH (person:PERSON {name: $person})-[r]-(book:BOOK) 
            RETURN person.name AS name, person.location AS location, type(r) AS r, book.name AS title""", person=person)
            if result is None:
                pprint("Person {person} not found".format(person=person))
            else:
                for record in result:
                    pprint(record['name'], record['location'], record['r'], record['title'])

    def find_book(self, title):
        with self.connection.session() as session:
            result = session.run("""MATCH (b:BOOK {name: $title})<-[r]-(p:PERSON) 
            RETURN b.name AS title, type(r) AS r, p.name AS name""", title=title)

            if result is None:
                pprint("Book {title} not found".format(title=title))
            else:
                for record in result:
                    pprint(record['name'], record['r'], record['title'])

    def create_book(self, params):
        query = 'CREATE (b:BOOK) SET b = $props RETURN b'
        with self.connection.session() as session:
            result = session.run(query, props= params)
            summary = result.consume()
            records_created = summary.counters.nodes_created
            print(f"Number of nodes created: {records_created}")

    def create_person(self, params):
        query = 'CREATE (b:PERSON) SET b = $props RETURN b'
        with self.connection.session() as session:
            result = session.run(query, props= params)
            summary = result.consume()
            records_created = summary.counters.nodes_created
            print(f"Number of nodes created: {records_created}")

    def create_relations(self):
        query = ["""MATCH 
                    (p:PERSON), (b:BOOK) WHERE p.name = 'Truman Capote' AND b.name = '1984'
                    MERGE (p)-[:READ]->(b)""", 
                """MATCH (p:PERSON), (b:BOOK) WHERE p.name = 'Truman Capote' AND b.name = 'Black Sea'
                    MERGE (p)-[:READ]->(b)""",
                """MATCH (p:PERSON), (b:BOOK) WHERE p.name = 'John Steinbeck' AND b.name = '1984'
                    MERGE (p)-[:READ]->(b)"""]
        counter = 0
        for q in query:
            with self.connection.session() as session:
                result = session.run(q)
                summary = result.consume()
                counter += summary.counters.relationships_created
        print(f"Number of relationships created: {counter}")


    def delete_nodes(self):
        query = ['MATCH (n:PERSON)-[r]-() DELETE n, r', 'MATCH (b:BOOK) DELETE b']
        counter = 0
        for q in query:
            with self.connection.session() as session:
                result = session.run(q)
                summary = result.consume()
                counter += summary.counters.nodes_deleted
        print(f"Number of nodes deleted: {counter}")

    def __del__(self):
        self.connection.close()

def main():
    db = CreateDB()
    pprint(db)
    books = [{"name": "1984", "author": "George Orwell"},
            {"name": "Black Sea", "author": "George Asherson"}]
    for params in books:
        db.create_book(params)
    people = [{"name": "John Steinbeck", "location": "New York"},
            {"name": "Truman Capote", "location": "Léos Angeles"}]
    for params in people:
        db.create_person(params)

    db.create_relations()
    print()
    db.find_book('1984')
    db.find_book('Black Sea')
    db.find_persons('John Steinbeck')
    # db.delete_nodes()
    del db

if __name__ == '__main__':
    main()