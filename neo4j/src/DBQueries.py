from neo4j import GraphDatabase, basic_auth
import rich as pprint
import yaml
import logging
from neo4j.exceptions import ServiceUnavailable, CypherSyntaxError
import networkx as nx
import matplotlib.pyplot as plt
from tabulate import tabulate
from colorama import Fore, Style

__author__    = "Author: PWLO"
__version__   = "Revision: 0.1"
__date__      = "Date: 2023-06-16 18:09:43 +0200 (Fr, 16 jun 2023)"
__license__   = 'public domain'
__copyright__ = "PWLO (c) 2023"

'''
TODO:
    - Full text search
    - Path finding:
        - Find shortest path between two nodes
        - Find all shortest paths between all nodes
        - Find all shortest paths between all nodes
    - Centrality analysis
    - Community detection
    - Similarity calculations
'''

class DBQueries:

    def __init__(self) -> None:
        # logging.basicConfig(filename='error.log', level=logging.ERROR)

        self.logger = logging.getLogger('error_logger')
        self.logger.setLevel(logging.ERROR)
        formatter = logging.Formatter('[%(asctime)s] %(levelname)s: %(message)s', '%Y-%m-%d %H:%M:%S')
        file_handler = logging.FileHandler('error.log')
        file_handler.setFormatter(formatter)            
        self.logger.addHandler(file_handler)

        self.loggerinfo = logging.getLogger('info_logger')
        self.loggerinfo.setLevel(logging.INFO)
        self.loggerinfo.addHandler(file_handler)

        file_path = 'access.yaml'

        with open(file_path, 'r') as file:
            yaml_data = yaml.load(file, Loader=yaml.FullLoader)

        uri = yaml_data['instance1']['uri']
        username = yaml_data['instance1']['username']
        password = yaml_data['instance1']['password']
        self.driver = GraphDatabase.driver(uri, auth=basic_auth(username, password))
        self.loggerinfo.info("Connected")
    
    def ShowCustomers(self, customer) -> None:
        with self.driver.session() as session:
            result = session.run(f"MATCH (n:{customer}) RETURN n.companyName, n.city LIMIT 25")
            for record in result:
                pprint.print(record['n.companyName'])
                pprint.print(record['n.city'])
                print('################\n')
    
    def ShowOrders(self, customer_id) -> None:
        query = """
        MATCH (customer:Customer {customerID: $customer_id})-[:PURCHASED]->(order:Order)
        RETURN order
        """
        try:
            with self.driver.session() as session:
                result = session.run(query, customer_id=customer_id)
                for record in result:
                    pprint.print(record)
        except ServiceUnavailable as exception:
            self.logger.error("{query} raised an error: \n {exception}".format(
                query=query, exception=exception))
        except CypherSyntaxError as exception:
            self.logger.error("{query} raised an error: \n {exception}".format(
                query=query, exception=exception))
            raise

    def ShowSuppliers(self) -> None:
        query = """
        MATCH (n:Product)<-[r:SUPPLIES]-(s:Supplier)
        RETURN n.productName, r, s.companyName, s.city
        LIMIT 25
        """
        try:
            data = [["Product Name", "Company Name", "City"]]
            with self.driver.session() as session:
                result = session.run(query)
                for record in result:
                    # pprint.print(record['n.productName'])
                    # pprint.print(record['s.companyName'])
                    # pprint.print(record['s.city'])
                    # print()
                    data.append([record['n.productName'],record['s.companyName'],record['s.city']])
            print()
            header_color = Fore.YELLOW
            data_color = Fore.CYAN
            reset_color = Style.RESET_ALL
            formatted_data = [[f"{header_color}{cell}{reset_color}" if index == 0 else f"{data_color}{cell}{reset_color}" for index, cell in enumerate(row)]
                for row in data
            ]
            table = tabulate(formatted_data, headers="firstrow", tablefmt="fancy_grid")
            print(table)
    
        except ServiceUnavailable as exception:
            self.logger.error("{query} raised an error: \n {exception}".format(
                query=query, exception=exception))
        except CypherSyntaxError as exception:
            self.logger.error("{query} raised an error: \n {exception}".format(
                query=query, exception=exception))
            raise

    def ShowProductsPerCustomer(self, customerName):
        query = """
        MATCH (c:Customer {companyName: $customerName})-[p:PURCHASED]->(o:Order)-[i:ORDERS]->(m:Product) RETURN c, m.productName, m.unitPrice
        """
        try:
            data = [["Product Name", "Price"]]
            with self.driver.session() as session:
                result = session.run(query, customerName=customerName)
                for record in result:
                    # pprint.print(record['m.productName'])
                    # pprint.print(record['m.unitPrice'])
                    # print()
                    data.append([record['m.productName'],record['m.unitPrice']])
            print()
            header_color = Fore.YELLOW
            data_color = Fore.CYAN
            reset_color = Style.RESET_ALL
            formatted_data = [[f"{header_color}{cell}{reset_color}" if index == 0 else f"{data_color}{cell}{reset_color}" for index, cell in enumerate(row)]
                for row in data
            ]
            table = tabulate(formatted_data, headers="firstrow", tablefmt="fancy_grid")
            print(table)
        except ServiceUnavailable as exception:
            self.logger.error("{query} raised an error: \n {exception}".format(
                query=query, exception=exception))
        except CypherSyntaxError as exception:
            self.logger.error("{query} raised an error: \n {exception}".format(
                query=query, exception=exception))
            raise

    def PathTraversal(self):
        query = """
            MATCH path = (Customer)-[*]->(Product)
            WHERE Customer.customerID = 'SUPRD'
            AND Product.productID = '20'
            RETURN path
            """
        with self.driver.session() as session:
            result = session.run(query)
            for record in result:
                path = record["path"]
                pprint.print(path)

    def ShowNetwork(self):
        query = """
            MATCH (c:Customer)-[r]->(o:Order)
            WHERE c.customerID = 'SUPRD'
            RETURN c, r, o
            """
        graph = nx.Graph()
        with self.driver.session() as session:
            result = session.run(query)
            for record in result:
                cust = record["c"]
                ord = record["o"]
                r = record["r"]
                
                graph.add_node(cust.element_id, label=cust["companyName"])
                graph.add_node(ord.element_id, label=ord["shipName"])
                graph.add_edge(cust.id, ord.element_id, label=r.type)                

            pos = nx.spring_layout(graph)
            labels = nx.get_node_attributes(graph, "label")
            edge_labels = nx.get_edge_attributes(graph, "label")

            nx.draw_networkx(graph, pos, labels=labels)
            nx.draw_networkx_edge_labels(graph, pos, edge_labels=edge_labels)

            plt.show()

    # Full-text search query
    def search_all_nodes_by_text(self, searchString):
        query = """
        MATCH (n) WHERE any(prop in keys(n) WHERE toString(n[prop]) CONTAINS $searchStr) RETURN n
        """
        with self.driver.session() as session:
            result = session.run(query, searchStr=searchString)
            for record in result:
                node = record["n"]
                print(node)

    def __del__(self) -> None:
        self.driver.close()

def main() -> None:
    dba = DBQueries()
    try:
        # dba.ShowCustomers('Customer')
    # 
        # dba.ShowOrders('ALFKI')
        dba.ShowProductsPerCustomer('Chop-suey Chinese')
        # dba.ShowSuppliers()
        # dba.PathTraversal()
        # dba.ShowNetwork()
        # dba.search_all_nodes_by_text('ALFKI')
    finally:
        dba = None
    
if __name__ == '__main__':
    main()
