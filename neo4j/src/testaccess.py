from neo4j import GraphDatabase, basic_auth
from rich import print as pprint
import yaml

file_path = 'access.yaml'

with open(file_path, 'r') as file:
    yaml_data = yaml.load(file, Loader=yaml.FullLoader)

uri = yaml_data['instance1']['uri']
username = yaml_data['instance1']['username']
password = yaml_data['instance1']['password']
driver = GraphDatabase.driver(uri, auth=basic_auth(username, password))

pprint(driver)